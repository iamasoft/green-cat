"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import atexit
import logging
import re
import subprocess
from injector import inject
from PyMata import pymata

from app.config import Config
from . import GreenCat


class RealGreenCat(GreenCat):
	"""
	Real implementation of the `GreenCat` interface that uses hardware resources of the device.
	"""

	@inject
	def __init__(self, cfg: Config):
		self.__logger = logging.getLogger(self.__class__.__name__)
		self.__cfg = cfg
		self.__board = pymata.PyMata(port_id=cfg.com_port, verbose=False)
		self.__board.set_pin_mode(0, pymata.PyMata.INPUT, pymata.PyMata.ANALOG)
		_ = self.sensors  # First reading is incorrect sometimes
		atexit.register(self.__board.close)

	def play_mp3(self, file_path: str) -> bool:
		self.__logger.info('Playing back MP3 file %s', file_path)
		return subprocess.run(['mpg123', '--quiet', '--no-control', file_path]).returncode == 0

	@property
	def sensor_soil_humidity(self) -> int:
		value = -1
		while value < 0 or value > 1024:
			# Sometimes the value exceeds 1024, which is error
			value = self.__board.analog_read(0)  # type: int
		# The `value` is between 0 and 1023 and inverted, so we need to convert it
		return int(((1023 - value) / 1024) * 100)

	@property
	def sensor_cpu_temperature(self) -> int:
		with open('/sys/devices/virtual/thermal/thermal_zone0/temp') as f:
			return int(f.read())

	@property
	def sensor_wifi_signal(self) -> int:
		result = subprocess.run(['iwconfig', self.__cfg.wifi_interface], stdout=subprocess.PIPE)
		if result.returncode == 0:
			found = re.findall('Signal level=(.+?)\s*dBm', str(result.stdout, 'ascii'))
			if len(found) == 1:
				return int(found[0])
			else:
				self.__logger.error('WiFi signal: Expected 1 entry, but got %d', len(found))
				return -1
		else:
			self.__logger.error('WiFi signal: Unable to execute `iwconfig` command')
			return -1
