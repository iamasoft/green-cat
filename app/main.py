"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging

from flask import Flask, Request, json
from flask_injector import FlaskInjector, Binder
from injector import SingletonScope
from werkzeug.exceptions import UnsupportedMediaType

from app.config import Config
from app.device import GreenCat
from app.meaw import Meaw

# Constructing the application instance
app = Flask(__name__, static_path='/static')


# Configuring API routes
@app.route('/')
def index():
	return app.send_static_file('index.html')


@app.route('/settings', methods=['GET'])
def get_settings(cfg: Config):
	return json.jsonify(cfg.settings)


@app.route('/settings', methods=['PUT'])
def set_settings(cfg: Config, req: Request):
	if req.is_json:
		cfg.settings = req.json
		return json.jsonify(cfg.settings)
	else:
		raise UnsupportedMediaType('Неподдерживаемый формат данных')


@app.route('/sensors', methods=['GET'])
def sensors(cat: GreenCat):
	return json.jsonify(cat.sensors)


# Configuring DI framework
def configure_injections(binder: Binder) -> None:
	"""
	Configure dependency injection bindings.

	:param binder:
		the DI `Binder` instance
	"""
	# Configuration
	cfg = Config()
	binder.bind(Config, cfg)
	# GreenCat interface
	binder.bind(GreenCat, cfg.green_cat_impl, scope=SingletonScope)


di = FlaskInjector(app, [configure_injections])  # type: FlaskInjector


def start():
	"""
	Start the application.
	"""
	cfg = di.injector.get(Config)  # type: Config
	cat = di.injector.get(GreenCat)  # type: GreenCat
	logging.basicConfig(format='%(asctime)s [%(levelname)-8s] %(name)s - %(message)s',
		level=logging.DEBUG if cfg.debug else logging.INFO)
	logging.getLogger('werkzeug').setLevel(logging.ERROR)
	meaw = Meaw(cat, cfg)
	try:
		meaw.start()
		app.run(host='0.0.0.0', port=cfg.http_port, threaded=True, debug=False, use_reloader=False)
	except KeyboardInterrupt:
		meaw.stop()
		meaw.join()
