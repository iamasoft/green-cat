"""
green-cat
Copyright (C) 2017  Quantum Dream Studio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import atexit
import datetime
import logging
import operator
import time
from threading import Thread
from typing import Callable

import tzlocal

from .config import Config
from .device import GreenCat


def time_in_range(since: datetime.time, until: datetime.time, x: datetime.time):
	"""
	:return: `True` if `x` is in the range `[since, until]`; `False` otherwise
	"""
	if since <= until:
		return since <= x <= until
	else:
		return since <= x or x <= until


class Meaw(Thread):
	"""
	Thread that performs standard Green Cat activities - checking sensors and meawing.
	"""

	def __init__(self, cat: GreenCat, cfg: Config):
		"""
		Construct the thread in inactive mode.
		:param cat:
			the `GreenCat` instance
		:param cfg:
			the application configuration
		"""
		super(Meaw, self).__init__(name='^._.^', daemon=True)
		self.__cat = cat
		self.__cfg = cfg
		self.__stopped = False
		self.__logger = logging.getLogger(self.name)
		atexit.register(self.stop)

	def run(self):
		"""
		Thread's activity. Checking sensors, issuing signals.
		"""
		self.__logger.info('Meawing Routine started. Purr!')
		while not self.__stopped:
			levels = sorted(self.__cfg.settings['signals'].values(), key=lambda l: l['soilHumidityLevel'])
			sensors = self.__cat.sensors
			soil_humidity = sensors['soil_humidity']
			self.__logger.debug('Current soil humidity level is %d%%', soil_humidity)
			for level in levels:
				op = getattr(operator, level['operator'], operator.le)  # type: Callable[[int, int]]
				if op(soil_humidity, level['soilHumidityLevel']):
					self.__meaw(level)
					break
			time.sleep(self.__cfg.settings['sensorsReadIntervalSec'])

	def stop(self):
		"""
		Stop this thread.
		"""
		self.__stopped = True
		self.__logger.info('Going to have some sleep *^-_-^* Love you!')

	def __meaw(self, level: dict):
		self.__logger.debug('I want to meaw this: %s', level)
		# Check if we are allowed to meaw!
		if self.__cfg.settings['quietMode']['on']:
			current_time = datetime.datetime.now(tzlocal.get_localzone()).time()
			since = datetime.datetime.strptime(self.__cfg.settings['quietMode']['since'], '%H:%M').time()
			until = datetime.datetime.strptime(self.__cfg.settings['quietMode']['until'], '%H:%M').time()
			if time_in_range(since, until, current_time):
				# It's quiet time now, don't meaw!
				self.__logger.debug('... but I have to be quiet!')
				return
		# Meawing!!1
		if not self.__cat.play_random_mp3(level['soundLibrary']):
			self.__logger.debug('... but I have no voice :(')
